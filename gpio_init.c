#define PWM_FREQUENCY 20000
#define SAMPLE_RATE 2

// Configure PWM sample rate for improved PWM frequency range
rc = gpioCfgClock(SAMPLE_RATE, 0, 0);

// Initialize Raspberry Pi GPIO subsystem
rc = gpioInitialise();
if ( rc < 0 ) {
	printf("Error: Unable to initialize GPIO\n");
	printf("\tNote: pigpiod must be stopped if it is running\n");
	return ERROR_GPIO;
}

// Configure GPIO pin for Motor A1 as output
rc = gpioSetMode(GPIO_PIN_MOTOR_A1, PI_OUTPUT);
if ( rc != 0 ) {
	printf("Error: Unable to set GPIO pin %d to output\n", GPIO_PIN_MOTOR_A1);
	return ERROR_GPIO;
}

// Configure GPIO pin for Motor A2 as output
rc = gpioSetMode(GPIO_PIN_MOTOR_A2, PI_OUTPUT);
if ( rc != 0 ) {
	printf("Error: Unable to set GPIO pin %d to output\n", GPIO_PIN_MOTOR_A2);
	return ERROR_GPIO;
}

// Configure GPIO pin for Motor B1 as output
rc = gpioSetMode(GPIO_PIN_MOTOR_B1, PI_OUTPUT);
if ( rc != 0 ) {
	printf("Error: Unable to set GPIO pin %d to output\n", GPIO_PIN_MOTOR_B1);
	return ERROR_GPIO;
}

// Configure GPIO pin for Motor B2 as output
rc = gpioSetMode(GPIO_PIN_MOTOR_B2, PI_OUTPUT);
if ( rc != 0 ) {
	printf("Error: Unable to set GPIO pin %d to output\n", GPIO_PIN_MOTOR_B2);
	return ERROR_GPIO;
}

// Set PWM frequency for Motor A
rc = gpioSetPWMfrequency(GPIO_PIN_MOTOR_A1, PWM_FREQUENCY);
if ( rc == PI_BAD_USER_GPIO ) {
	printf("Error: Unable to set PWM frequency on pin %d to %d\n", GPIO_PIN_MOTOR_A1, PWM_FREQUENCY);
	return ERROR_GPIO;
} else {
	printf("Info: PWM frequency for Motor A set to %d\n", rc);
}

// Set PWM frequency for Motor B
rc = gpioSetPWMfrequency(GPIO_PIN_MOTOR_B1, PWM_FREQUENCY);
if ( rc == PI_BAD_USER_GPIO ) {
	printf("Error: Unable to set PWM frequency on pin %d to %d\n", GPIO_PIN_MOTOR_B1, PWM_FREQUENCY);
	return ERROR_GPIO;
} else {
	printf("Info: PWM frequency for Motor B set to %d\n", rc);
}

