#define GPIO_PIN_MOTOR_A1 18 // PWM
#define GPIO_PIN_MOTOR_A2 17 // Digital
#define GPIO_PIN_MOTOR_B1 19 // PWM
#define GPIO_PIN_MOTOR_B2 16 // Digital

#define TOUCH_AXIS_INVERT 1
#define TOUCH_AXIS_MIN -32768
#define TOUCH_AXIS_MAX  32768


// Prototypes
void  setSpeedMotor(char side, float speed);
int   speed2dutycycle(float speed);
float touch2relative(__s32 value);

// side A:0, B:1
// Speed in range [-1, 1]
// Returns speed that was set
void setSpeedMotor(char side, float speed)
{
	int rc;
	char name[2];
	unsigned pin_pwm;
	unsigned pin_dir;
	unsigned direction;

	// Set name and pin numbers based on indicated side
	if ( side == 0 ) {
		strcpy(name, "A");
		pin_pwm = GPIO_PIN_MOTOR_A1;
		pin_dir = GPIO_PIN_MOTOR_A2;
	} else if ( side == 1 ) {
		strcpy(name, "B");
		pin_pwm = GPIO_PIN_MOTOR_B1;
		pin_dir = GPIO_PIN_MOTOR_B2;
	} else {
		printf("Error: Invalid motor side %d\n", side);
		return;
	}

	// Validate requested speed
	if ( ( speed < -1.0f ) || ( speed > 1.0f ) ) {
		printf("Error: Invalid speed setting for motor %s: %f\n", name, speed);
		return;
	}

	// Set direction (0.0V reverse, 3.3V for forward)
	direction = ( speed <= 0.0f ) ? 0 : 1;
	rc = gpioWrite(pin_dir, direction);
	if ( rc != 0 ) {
		printf("Error: Unable to set motor %s2 output (return code %d)\n", name, rc);
		return;
	}

	// Set PWM duty cycle within range [0, 255]
	rc = gpioPWM(pin_pwm, speed2dutycycle(speed));
	if ( rc != 0 ) {
		printf("Error: Unable to set motor %s1 PWM (return code %d)\n", name, rc);
		return;
	}
}

/*  speed2dutycycle(speed)
 *    Convert a relative speed to 8-bit duty cycle value. Note that required PWM
 *    duty cycle increases with forward speed, but decreases with negative speed.
 *  Arguments:
 *    float speed - Relative speed within range [-1.0f, 1.0f]
 *  Returns:
 *    An int duty cycle value in range [0, 256]
 */
int speed2dutycycle(float speed)
{
	// Clamp speed to range [-1.0, 1.0]
	if ( speed < -1.0f ) {
		speed = -1.0f;
	} else if ( speed > 1.0f ) {
		speed =  1.0f;
	}

	// Ensure speed 0.0 yields 0 duty cycle
	// Map range [-1,0) -> range [0,1]
	if ( speed == 0.0f ) {
		return 0;
	} else if ( speed < 0.0f ) {
		speed += 1.0f;
	}

	// Map [0.0f, 1.0f] -> [255, 0]
	return (int)(255.0f * (1.0f - speed));
}

/*  touch2relative(value)
 *    Convert a Steam Controller's touch axis output value to a relative number.
 *  Arguments:
 *    __s32 value - the input_event value for an EV_ABS from a touch axis
 *  Returns:
 *    A relative value in the range [-1.0f, 1.0f]
 */
// Map controller touch axis to range [-1,1]
float touch2relative(__s32 value)
{
	// Clamp value to range [-max, max]
	if ( value < TOUCH_AXIS_MIN ) {
		value = TOUCH_AXIS_MIN;
	} else if ( value > TOUCH_AXIS_MAX ) {
		value = TOUCH_AXIS_MAX;
	}

	// Invert axis if flag is set
	if ( TOUCH_AXIS_INVERT )
		value *= -1;

	// Scale speed to range [-1, 1]
	return ((float)value / (float)TOUCH_AXIS_MAX);
}

// Take action based on an event
int action(struct input_event *event, struct SteamController *controller)
{
	// Ignore any input_event's not from a button or axis
	if ( (event->type != EV_ABS) && (event->type != EV_KEY) )
		return 0;

	// Take action based on the button/axis
	switch (event->code) {
	case SC_AXIS_LTOUCH_Y:
		// Set speed for motor A
		setSpeedMotor(0, touch2relative(event->value));
		break;
	case SC_AXIS_RTOUCH_Y:
		// Set speed for motor B
		setSpeedMotor(1, touch2relative(event->value));
		break;
	case SC_BUTTON_STEAM:
		// Set speeds to zero, return a halt flag
		setSpeedMotor(0, 0);
		setSpeedMotor(1, 0);
		return -1;
	default:
		break;
	}

	return 0;
}

