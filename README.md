# zumoctl

**zumoctl** is an open source robotics application that provides an interface
for controlling a robot via a Raspberry Pi and a Steam Controller.

Root privilege is currently required to enable the pigpio library to manage the
Raspberry Pi's low-level GPIO hardware.

## Usage

```# zumoctl [controller_handle] [opts]```

The controller handle argument is mandatory and can be located using the
description in the **Steam Controller** section below.

Running zumoctl without any optional arguments generates minimal IO and thus
reduces CPU overhead. Optional arguments are as follows:
* `-v` or `--verbose`: pretty-prints every input event to stdout
* `--raw`: prints the raw data from every input event to stdout

## Compatibility & Dependencies

This application is presently confirmed to work on:
* Raspberry Pi Zero W (Raspbian)

No obvious compatibility issues exist with other Raspberry Pi boards, but they
have not yet been tested.

The [pigpio C interface](http://abyz.me.uk/rpi/pigpio/cif.html) must be installed
for this program to run. If not already present, the package `pigpio` can be
located through the default Raspbian package manager.

## Steam Controller

This application interfaces with a Steam Controller through the controller's
USB dongle and a C library that utilizes the Linux evdev interface. When a Steam
Controller is paired with the dongle it will present a symlink to an input event
handle with the following pattern:

```/dev/input/by-id/usb-Valve_Software_Steam_Controller-if**-event-joystick```

Support for the Steam Controller was added in the Linux 4.18 kernel version.
However, please note that Raspbian did not fully integrate this support as of
the May 2020 release, despite using the 4.19 kernel version. The Raspbian
2020-08-20 release has advanced the kernel version to 5.4 LTS and should fully
support the Steam controller, but this has not yet been confirmed.

