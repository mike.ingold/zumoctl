#include <stdio.h>     // printf
#include <stdlib.h>    // malloc
#include <string.h>    // strcmp
#include <pigpio.h>    // Raspberry Pi GPIO library
#include <math.h>      // round

#include "steamcontroller/SteamController.h"
#include "gpio.h"

#define ERROR_EARLY -1
#define ERROR_GPIO  -2

// Main
int main(int argc, char *argv[])
{
	char MODE;
	FILE *fp;
	struct SteamController controller;
	struct input_event *recvd;
	int rc;

	// Validate argument structure
	if ( ( argc < 2 ) || ( argc > 3 ) ) {
		printf("Usage: %s [evdev_handle] [opt]", argv[1]);
		return ERROR_EARLY;
	}

	// Parse optional argument
	if ( argc == 3) {
		if ( strcmp(argv[2],"--raw") == 0 ) {
			MODE = MODE_DISPLAY_RAW;
		} else if ( strcmp(argv[2],"--verbose") == 0 ) {
			MODE = MODE_DISPLAY_VERBOSE;
		} else if ( strcmp(argv[2],"-v") == 0 ) {
			MODE = MODE_DISPLAY_VERBOSE;
		} else {
			printf("Error: optional argument %s not recognized\n", argv[2]);
			return ERROR_EARLY;
		}
	} else {
		MODE = MODE_DISPLAY_QUIET;
	}

	// Connect to controller
	controller = SteamController();
	fp = fopen(argv[1], "rb");
	if ( fp == NULL ) {
		// Handle could not be opened
		printf("Error: unable to open %s\n", argv[1]);
		return ERROR_EARLY;
	}

	// Initialize GPIO
	#include "gpio_init.c"

	// Allocate memory for received input_event
	recvd = (struct input_event *)malloc(EVENT_SIZE);

	// Event loop
	printf("Monitoring for events\n");
	while ( recvd != NULL ) {
		// Monitor (blocking read) for next event
		getinputevent(recvd, fp);

		if ( recvd->time.tv_sec == 0 ) {
			// Exit loop if disconnect occurs
			printf("Error: Controller disconnected\n");
			break;
		} else {
			// Update the controller state and display the event
			update(&controller, recvd);
			display(recvd, MODE);

			// Take action based on the event specifics
			rc = action(recvd, &controller);
			if ( rc != 0 ) break;
		}
	}

	// Exit gracefully
	gpioTerminate();
	fclose(fp);
	return 0;
}

